package com.fioalpha.platform.network

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.math.BigInteger
import java.security.MessageDigest

internal fun providerRetrofit(
    client: OkHttpClient,
    url: String,
    gson: Gson
) = Retrofit.Builder()
    .client(client)
    .baseUrl(url)
    .addConverterFactory(GsonConverterFactory.create(gson))
    .build()

internal fun providerHttpClient(
    vararg interceptor: Interceptor
): OkHttpClient {
    return OkHttpClient.Builder()
        .apply {
            interceptor.forEach {
                this.addInterceptor(it)
            }
        }
        .build()
}

internal fun providerLoggingInterceptor() = HttpLoggingInterceptor().apply {
    this.level = HttpLoggingInterceptor.Level.BODY
}

internal fun providerGson() = GsonBuilder().create()

fun providerHttpClient(
    baseUrl: String
): Retrofit {
    return providerRetrofit(
        client = providerHttpClient(
            providerLoggingInterceptor(),
            AuthenticationInterceptor()
        ),
        url = baseUrl,
        gson = providerGson()
    )
}


class AuthenticationInterceptor: Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val newUrl = request.url.newBuilder()
            .addQueryParameter(
                "hash",
                AuthenticationHash.create(
                    System.currentTimeMillis(),
                    "",
                    ""
                )
            )

        val newRequest = request.newBuilder()
            .url(newUrl.build())
            .build()
        return chain.proceed(newRequest)
    }

}

object AuthenticationHash {
    fun create(
        timeStamp: Long,
        privateKey: String,
        publicKey: String
    ): String {
        val seed = "$timeStamp$privateKey$publicKey"
        return MessageDigest.getInstance("MD5")
            .run {
                BigInteger(1, this.digest(seed.toByteArray())).toString(16)
                    .padStart(32, '0')
            }
    }
}

